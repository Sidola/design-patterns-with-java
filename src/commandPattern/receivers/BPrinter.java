package commandPattern.receivers;

public class BPrinter {

	public void B_Print(String s) {
		System.out.println("-- BPrinter printing --");
		System.out.println(s);
	}
	
}
