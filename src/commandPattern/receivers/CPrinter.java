package commandPattern.receivers;

public class CPrinter {

	public void C_Print(String s) {
		System.out.println("-- CPrinter printing --");
		System.out.println(s);
	}
	
}
