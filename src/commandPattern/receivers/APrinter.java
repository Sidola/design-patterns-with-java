package commandPattern.receivers;

public class APrinter {

	public void A_Print(String s) {
		System.out.println("-- APrinter printing --");
		System.out.println(s);
	}
	
}
