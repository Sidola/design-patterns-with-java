package commandPattern;

import commandPattern.commands.APrintCommand;
import commandPattern.commands.BPrintCommand;
import commandPattern.commands.CPrintCommand;
import commandPattern.invokers.PrintMachine;
import commandPattern.receivers.APrinter;
import commandPattern.receivers.BPrinter;
import commandPattern.receivers.CPrinter;

public class Run {
	public static void main(String[] args) {
		PrintMachine myPrinter = new PrintMachine();

		myPrinter.setCommand( new APrintCommand( new APrinter() ) );	
		myPrinter.setCommand( new BPrintCommand( new BPrinter() ) );
		myPrinter.setCommand( new CPrintCommand( new CPrinter() ) );
		
		myPrinter.printData(0, "Hello world!");
		myPrinter.printData(1, "Hello world!");
		myPrinter.printData(2, "Hello world!");
	}
}
