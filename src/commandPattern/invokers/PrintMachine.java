package commandPattern.invokers;

import java.util.ArrayList;
import java.util.List;

import commandPattern.commands.Command;

public class PrintMachine {
	
	// Holds a list of commands
	List<Command> printCommands = new ArrayList<Command>();

	/**
	 * Stores a command to be executed later.
	 * 
	 * @param c - An instance of command
	 */
	public void setCommand(Command c) {
		printCommands.add(c);
	}
	
	/**
	 * Runs the execute() method on the selected command.
	 * 
	 * @param printerIndex - Index of the command-instance to execute
	 * @param s - Data to print
	 */
	public void printData(int printerIndex, String s) {
		if (printCommands.contains( printCommands.get(printerIndex) )) {
			printCommands.get(printerIndex).execute(s);
		}
	}
}
