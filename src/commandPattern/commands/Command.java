package commandPattern.commands;

public interface Command {

	public void execute(String s);
	public void undo();
	
}
