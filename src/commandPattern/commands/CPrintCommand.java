package commandPattern.commands;

import commandPattern.receivers.CPrinter;

public class CPrintCommand implements Command {

	CPrinter printer;
	
	public CPrintCommand(CPrinter printer) {
		this.printer = printer;
	}
	
	@Override
	public void execute(String s) {
		printer.C_Print(s);
	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub
	}

}
