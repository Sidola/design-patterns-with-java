package commandPattern.commands;

import commandPattern.receivers.BPrinter;

public class BPrintCommand implements Command {

	BPrinter printer;
	
	public BPrintCommand(BPrinter printer) {
		this.printer = printer;
	}
	
	@Override
	public void execute(String s) {
		printer.B_Print(s);		
	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub
	}

}
