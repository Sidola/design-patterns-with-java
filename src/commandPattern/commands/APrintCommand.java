package commandPattern.commands;

import commandPattern.receivers.APrinter;

public class APrintCommand implements Command {

	APrinter printer;
	
	public APrintCommand(APrinter printer) {
		this.printer = printer;
	}

	@Override
	public void execute(String s) {
		printer.A_Print(s);	
	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub
	}	
}
