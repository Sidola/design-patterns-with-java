package decoratorPattern.abstractComponents;

/**
 * An abstract component.
 * 
 * This class is an abstract version of the components we'll be making.
 * It holds a few instance variables and provides a list of methods the
 * concrete components will have to implement.
 */
public abstract class StringPrinter {
	
	protected String printerType = "Unkown";
	protected String indentation = "";
	
	public abstract void printString(String s);
	public abstract String getPrinterType();
	public abstract void setIndentation(String s);	
}
