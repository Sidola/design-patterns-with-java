package decoratorPattern.abstractDecorators;

import decoratorPattern.abstractComponents.StringPrinter;

/**
 * This is the abstract decorator.
 * It takes care of storing a reference to the StringPrinter we'll be wrapping.
 * 
 * It also provides default-implementations for the getPrinterType() and setIndentation() methods
 * so that concrete components don't have to bother with those unless they want to.
 */
public abstract class StringPrinterDecorator extends StringPrinter {
	
	protected StringPrinter sp;
	
	public StringPrinterDecorator(StringPrinter sp) {
		this.sp = sp;
	}
	
	@Override
	public abstract void printString(String s);
	
	@Override
	public String getPrinterType() {
		return this.sp.getPrinterType();
	}
	
	@Override
	public void setIndentation(String s) {
		this.sp.setIndentation(s);
	}
}
