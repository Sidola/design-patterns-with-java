package decoratorPattern;

import decoratorPattern.abstractComponents.StringPrinter;
import decoratorPattern.concreteComponents.LinebreakStringPrinter;
import decoratorPattern.concreteComponents.NoLinebreakStringPrinter;
import decoratorPattern.concreteDecorators.CommaSeparatedStringPrinter;
import decoratorPattern.concreteDecorators.UnderscoreSeparatedStringPrinter;

public abstract class Run {
	
	public static void main(String[] args) {
		
		// Create a basic LinebreakStringPrinter and print two lines
		StringPrinter sp = new LinebreakStringPrinter();
		sp.printString("This is line one!");
		sp.printString("This is line two!");
		
		bigLinebreak();
		
		// Create a basic NoLinebreaksStringPrinter and print two lines
		sp = new NoLinebreakStringPrinter();
		sp.printString("This is line one.");
		sp.printString("This is line two.");
		
		bigLinebreak();
		
		// Decorate a LineBreakStringPrinter in a CommaSeparatedStringPrinter
		sp = new CommaSeparatedStringPrinter( new LinebreakStringPrinter() );
		sp.setIndentation("\t"); // Set some indentation
		
		sp.printString("This is one line with commas.");
		sp.printString("This is two lines with commas.");
		
		bigLinebreak();
		
		// Decorate a NoLinebreakSpringPrinter in a CommaSeparatedStringPrinter
		sp = new CommaSeparatedStringPrinter( new NoLinebreakStringPrinter() );
		sp.setIndentation("--> "); // Set some indentation
		
		sp.printString("One line, commas.");
		sp.printString("Two lines, commas!");

		bigLinebreak();
		
		// This does not use an abstractDecorator, but still works?
		sp = new UnderscoreSeparatedStringPrinter( new LinebreakStringPrinter() );
		sp.printString("So why do we need an abstract decorator?");
				
	}	
	
	private static void bigLinebreak() {
		// Prints some line-breaks for readability
		System.out.println();
		System.out.println();
		System.out.println();
	}	
}

