package decoratorPattern.concreteComponents;

import decoratorPattern.abstractComponents.StringPrinter;

/**
 * A concrete component.
 * 
 * This version prints text with a trailing line-break.
 */
public class LinebreakStringPrinter extends StringPrinter {

	@Override
	public void printString(String s) {
		System.out.println(super.indentation + s);		
	}	
	
	@Override
	public String getPrinterType() {
		return "LinebreakStringPrinter";
	}

	@Override
	public void setIndentation(String s) {
		super.indentation = s;
	}
}
