package decoratorPattern.concreteComponents;

import decoratorPattern.abstractComponents.StringPrinter;

/**
 * A concrete component.
 * 
 * This version prints text without trailing line-breaks.
 */
public class NoLinebreakStringPrinter extends StringPrinter {

	@Override
	public void printString(String s) {
		System.out.print(super.indentation + s);		
	}

	@Override
	public String getPrinterType() {
		return "NoLinebreakStringPrinter";
	}
	
	@Override
	public void setIndentation(String s) {
		super.indentation = s;
	}	
}
