package decoratorPattern.concreteDecorators;

import decoratorPattern.abstractComponents.StringPrinter;

/**
 * This is an example of a concrete decorator that extends directly
 * from the abstract component.
 * 
 * This may seem like it would make sense for a small example like this,
 * but once you start adding more decorators you'll notice that you'll have to
 * implement a bunch of methods that you don't care about.
 * 
 * That's where the abstract decorator comes in and implements all methods
 * once with a default implementation that can later be overwritten if needed.
 */
public class UnderscoreSeparatedStringPrinter extends StringPrinter {

	protected StringPrinter sp;
	
	/**
	 * Grab a reference to whichever StringPrinter we'll be using.
	 * @param sp
	 */
	public UnderscoreSeparatedStringPrinter(StringPrinter sp) {
		this.sp = sp;
	}
	
	/**
	 * Override the printSpring method and implement our own code.
	 * Once we're done, pass the modified string to our referenced StringPrinter.
	 */
	@Override
	public void printString(String s) {
		char[] charArray = s.toCharArray();
		String underscoreSeparated = "";
		
		// Add the first letter
		underscoreSeparated = underscoreSeparated + charArray[0];

		// Add the remaining letters
		for (int i = 1; i < charArray.length; i++) {
			underscoreSeparated = underscoreSeparated + "_" + charArray[i];
		}

		// Pass the string along.
		sp.printString(underscoreSeparated);
	}

	/**
	 * Since this is not extending an abstract decorator, we'll have
	 * to implement these methods too, even though we're not using or modifying them.
	 * 
	 * Now imagine having to do this for EVERY concrete decorator.
	 */
	
	@Override
	public String getPrinterType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setIndentation(String s) {
		// TODO Auto-generated method stub
	}
}
