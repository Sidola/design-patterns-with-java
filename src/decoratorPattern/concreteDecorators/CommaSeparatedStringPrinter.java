package decoratorPattern.concreteDecorators;

import decoratorPattern.abstractComponents.StringPrinter;
import decoratorPattern.abstractDecorators.StringPrinterDecorator;

/**
 * This concrete decorator extends an abstract decorator. 
 * As such it only has to implement whatever method is absolutely necessary
 * and can skip the rest.
 */
public class CommaSeparatedStringPrinter extends StringPrinterDecorator {
	
	/**
	 * Take a StringPrinter reference and pass it along to our abstract decorator.
	 * @param sp
	 */
	public CommaSeparatedStringPrinter(StringPrinter sp) {
		super(sp);
	}
	
	/**
	 * Implement the printString method and make modifications.
	 */
	@Override
	public void printString(String s) {	
	
		char[] charArray = s.toCharArray();
		String commaSeparated = "";
		
		// Add the first letter
		commaSeparated = commaSeparated + charArray[0];

		// Add the remaining letters
		for (int i = 1; i < charArray.length; i++) {
			commaSeparated = commaSeparated + "," + charArray[i];
		}
		
		// Hand over the string to whoever is printing it
		super.sp.printString(commaSeparated);
	}
}
