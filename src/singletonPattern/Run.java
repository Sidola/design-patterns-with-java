package singletonPattern;

public class Run {

	public static void main(String[] args) {
		
		// Store an instance in sObject and set/get some text
		mySingleton sObject = mySingleton.getInstance();
		
		sObject.setText("I'm setting some text");
		System.out.println( sObject.getText() );
		
		// Store an instance in oObject and get/set the same text
		mySingleton oObject = mySingleton.getInstance();
		
		System.out.println( oObject.getText() );
		oObject.setText("Now oObject sets the text");
		
		
		System.out.println();
		
		
		// Get text from both objects to see that they're calling the same instance
		System.out.println( sObject.getText() );
		System.out.println( oObject.getText() );
	}
}
