package singletonPattern;

public class mySingleton {

	// The instance variable that will hold the singleton
	private static mySingleton uniqueSingleton;
	
	// Private constructor, class cannot be instantiated from the outside
	private mySingleton() {}
	
	// Public static method to get an instance of mySingleton
	public static mySingleton getInstance() {
		if (uniqueSingleton == null) {
			uniqueSingleton = new mySingleton();
		}
		
		return uniqueSingleton;
	}
	
	// Insert some actually useful code for the singleton to perform below...
	
	private String text = "No text here";
	
	public void setText(String s) {
		text = s;
	}
	
	public String getText() {
		return text;
	}
	
}
