package strategyPattern.behaviors;

/**
 * A tabbed printer.
 * Indents the message with a tab.
 */
public class TabbedPrinter implements PrintBehavior {

	public void print(String s) {
		System.out.println("Msg:\t" + s);
	}
	
}
