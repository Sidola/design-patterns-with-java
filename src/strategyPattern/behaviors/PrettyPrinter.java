package strategyPattern.behaviors;

/**
 * A pretty-printer.
 * Prefixes the string with a message before it prints it.
 */
public class PrettyPrinter implements PrintBehavior {

	public void print(String s) {
		System.out.println("Look at my pretty message: " + s);
	}
	
}
