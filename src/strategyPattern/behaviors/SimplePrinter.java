package strategyPattern.behaviors;

/**
 * A simple printer.
 * Prints a string as it is.
 */
public class SimplePrinter implements PrintBehavior {

	public void print(String s) {
		System.out.println(s);
	}
	
}
