package strategyPattern.behaviors;

/**
 * The interface for all printing behaviors.
 */
public interface PrintBehavior {

	public void print(String s);
	
}
