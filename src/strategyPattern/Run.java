package strategyPattern;

import strategyPattern.behaviors.PrettyPrinter;
import strategyPattern.behaviors.PrintBehavior;
import strategyPattern.behaviors.SimplePrinter;
import strategyPattern.behaviors.TabbedPrinter;

public class Run {

	private PrintBehavior printer;
	
	/**
	 * Main.
	 * @param args
	 */
	public static void main(String[] args) {
		Run myObj = new Run();
		
		// Try different printing behaviors
		
		myObj.setPrinter( new SimplePrinter() );
		myObj.printer.print("Printing a string using SimplePrinter!");
		
		myObj.setPrinter( new PrettyPrinter() );
		myObj.printer.print("Printing a string using PrettyPrinter!");
		
		myObj.setPrinter( new TabbedPrinter() );
		myObj.printer.print("Printing a string using TabbedPrinter!");	
	}
	
	/**
	 * Sets a new PrintBehavior.
	 * 
	 * @param ph - An instance of a PrintBehavior.
	 */
	public void setPrinter(PrintBehavior ph) {
		printer = ph;
	}
	
}
