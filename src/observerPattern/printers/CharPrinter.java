package observerPattern.printers;

import observerPattern.interfaces.Observer;

/**
 * This class is an observer
 * that takes data and prints it with sticks between each word.
 */
public class CharPrinter implements Observer {

	@Override
	public void update(String s) {
		System.out.print("Data received: |");
		for (int i = 0; i < s.length(); i++) {
			System.out.print(s.charAt(i) + " | ");
		}
		System.out.println();
	}

	
	
}
