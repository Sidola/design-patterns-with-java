package observerPattern.printers;

import observerPattern.interfaces.Observer;

/**
 * This class is an observer
 * that takes data and prints it as a simple string.
 */
public class StringPrinter implements Observer {

	@Override
	public void update(String s) {
		System.out.println("Data recieved: " + s);
	}

}
