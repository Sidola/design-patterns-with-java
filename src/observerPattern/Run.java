package observerPattern;

import observerPattern.printers.CharPrinter;
import observerPattern.printers.StringPrinter;

public class Run {

	DataGenerator generator = new DataGenerator();
	StringPrinter stringPrinter = new StringPrinter();	
	CharPrinter charPrinter = new CharPrinter();
	
	public static void main(String[] args) {
		Run myObj = new Run();
		
		// Register the stringPrinter as an observer
		myObj.generator.registerObserver(myObj.stringPrinter);
		
		// Set some data
		myObj.generator.setDataPackage("Bananas are yellow!");
		
		// Register the charPrinter as an observer
		myObj.generator.registerObserver(myObj.charPrinter);
		
		// Set some more data
		myObj.generator.setDataPackage("Now I've got two observers.");
		
		// Remove the stringPrinter from the observer list
		myObj.generator.removeObserver(myObj.stringPrinter);
		
		// Set some final data
		myObj.generator.setDataPackage("And now we're back to one observer.");
	}
	
}
