package observerPattern.interfaces;

/**
 * Subject interface. 
 */
public interface Subject {

	/**
	 * Registers a new observer.
	 * 
	 * @param o - Observer to register.
	 */
	public void registerObserver(Observer o);
	
	/**
	 * Removes an observer.
	 * 
	 * @param o - Observer to remove.
	 */
	public void removeObserver(Observer o);
	
	/**
	 * Notifies all registered observers.
	 */
	public void notifyObservers();
	
}
