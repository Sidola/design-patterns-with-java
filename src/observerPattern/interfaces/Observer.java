package observerPattern.interfaces;

/**
 * Observer interface.
 */
public interface Observer {
	
	/**
	 * Takes an update from the Subject.
	 * 
	 * @param s - a String with the new data
	 */
	public void update(String s);
	
}
