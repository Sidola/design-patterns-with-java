package observerPattern;

import java.util.ArrayList;
import java.util.List;

import observerPattern.interfaces.Observer;
import observerPattern.interfaces.Subject;

/**
 * This class is responsible for being the subject
 * and sending out data to all observers. 
 */
public class DataGenerator implements Subject {

	List<Observer> observerList = new ArrayList<Observer>();
	String dataPackage;	
	
	/**
	 * Adds an observer.
	 * 
	 * @param o - Instance of the observer to be added.
	 */
	@Override
	public void registerObserver(Observer o) {
		observerList.add(o);		
	}

	/**
	 * Removes an observer.
	 * 
	 * @param o - Instance of the observer to be removed.
	 */
	@Override
	public void removeObserver(Observer o) {
		if (observerList.contains(o)) {
			observerList.remove(o);
		}		
	}

	/**
	 * Notifies all observers.
	 */
	@Override
	public void notifyObservers() {
		for (Observer observer : observerList) {
			observer.update(dataPackage);
		}
	}
	
	/**
	 * Called whenever there is new data to be sent
	 * to the observers.
	 */
	private void newDataSet() {
		notifyObservers();
	}
	
	/**
	 * Sets a new value for the dataPackage.
	 * 
	 * @param s - New data.
	 */
	public void setDataPackage(String s) {
		dataPackage = s;
		newDataSet();
	}
	
	
}
